# MILOU
MILOU is a Monte Carlo generator for deeply virtual Compton scattering (DVCS), ep → eY$`\gamma`$. It is based on generalised parton distributions (GPDs) evolved to next-to-leading order.

See E. Perez, L. Schoeffel and L. Favart, "MILOU: a Monte-Carlo for Deeply Virtual Compton Scattering"
[hep-ph/0411389v1](https://arxiv.org/abs/hep-ph/0411389v1).

# Installation

Can be built using "make".
The "install" target should be customized to your environment before using.
Please note that 64 bit support is at this point not working.

Ignore warnings of the form:
```
      ASSIGN 100 TO LABEL                                               
Warning: Deleted feature: ASSIGN statement at (1)
```
This feature is deleted in F95. 

# CHANGES 

Specified return type 
```
 real*8 function  sf2test(xin,q2in)
```
in line 5173 of mcdvcsv1.f. 

Note: The original directory contained a sub-directory called nlo which is not currently used and not included in the git repo.


# Running
Note that the executables expect the .dat files and the dvcs.steer file
in the directory of execution. Easiest way to achieve this is a softlink (adapt to your location)
```sh
cp $EICDIRECTORY/PACKAGES/milou/dvcs.steer .
ln -s $EICDIRECTORY/PACKAGES/milou/*.dat .
```

You can then test with:
```sh
$EICDIRECTORY/bin/milou
```

# Known Issues
CANNOT cope with 64 bits. This is most likely fixable (it does compile, but the output contains NaNs).

